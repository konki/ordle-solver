from dataclasses import dataclass


@dataclass(slots=True)
class CharNum:
    """Class to store position and value of a character in a string"""

    char: str
    num: int


def count_char(c: str, string: str) -> int:
    """Count number of occurances of a char c in string"""
    return string.count(c)


def get_uppercase_pos(string) -> list[CharNum]:
    """Find the positions of all uppercase chars"""
    char_pos_list: list[CharNum] = []

    for i, c in enumerate(string):
        if c.isupper():
            char_pos_list.append(CharNum(char=c.lower(), num=i))
    return char_pos_list


def remove_nonalpha_chars(string) -> str:
    """Removes characters from string which are not alphabetical"""
    string_without_specials = ""
    for c in string:
        if c.isalpha():
            string_without_specials += c
    return string_without_specials


def valid_lengths(length: int, string_list: list[str]) -> list[str]:
    """Return list of strings with given length"""
    return [x.lower() for x in string_list if len(x) == length]


def fixed_chars_correct(char_positions: list[CharNum], string: str) -> bool:
    """Return whether or not all fixed chars a re in correct place"""
    for char_pos in char_positions:
        if string[char_pos.num] != char_pos.char:
            return False
    return True


def char_counts_correct(char_counts: list[CharNum], string: str) -> bool:
    """Return whether or not the number of characters in a string is correct"""
    for char_count in char_counts:
        # Check that number of required characters is correct
        if count_char(c=char_count.char, string=string) < char_count.num:
            return False
    return True
