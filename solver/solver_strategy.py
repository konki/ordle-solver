from dataclasses import dataclass
from typing import Protocol

from utils import strings

from .permuter import Permuter


class SolverStrategy(Protocol):
    def solve(self, wordlist: list[str]) -> list[str]:
        ...


@dataclass
class PermutationStrategy:
    permuter: Permuter
    input_string: str

    def solve(self, wordlist: list[str]) -> list[str]:
        return list(
            set(
                [
                    x
                    for x in list(self.permuter.permute(self.input_string.lower()))
                    if x in wordlist
                ]
            )
        )


@dataclass
class ContainsStrategy:
    legals: str = ""
    illegals: str = ""

    def solve(self, wordlist: list[str]) -> list[str]:
        word_length = len(self.legals)
        # Find candidate words with correct length
        candidates = strings.valid_lengths(length=word_length, string_list=wordlist)

        # Find positions and characters that have been marked with fixed positions through capitalization
        fixed_char_positions: list[strings.CharNum] = strings.get_uppercase_pos(
            self.legals
        )

        # Remove all nonalpha characters from legals string
        legals_lower_alpha: str = strings.remove_nonalpha_chars(self.legals).lower()
        # Count occurance of each character in legals string
        char_counts: list[strings.CharNum] = [
            strings.CharNum(
                char=c, num=strings.count_char(c=c, string=legals_lower_alpha)
            )
            for c in list(set(legals_lower_alpha))
        ]

        valids: list[str] = []
        for candidate in candidates:
            # Check if candidate containts illegal characters
            if (
                len(set(candidate).intersection(set(self.illegals))) == 0
                and strings.fixed_chars_correct(
                    char_positions=fixed_char_positions, string=candidate
                )
                and strings.char_counts_correct(
                    char_counts=char_counts, string=candidate
                )
            ):
                valids.append(candidate)

        return valids
