from dataclasses import dataclass

from .solver_strategy import SolverStrategy


@dataclass(slots=True)
class OrdleSolver:
    solver: SolverStrategy
    wordlist: set[str]

    def find_valids(self) -> list[str]:
        return self.solver.solve(wordlist=self.wordlist)
