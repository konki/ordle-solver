from itertools import permutations
from typing import Protocol, Tuple


class Permuter(Protocol):
    def permute(self, input_string: str) -> list[str]:
        ...


class StringPermuter:
    def permute(self, input_string: str) -> list[str]:
        string_permute_tuple: list[Tuple[str]] = list(permutations(input_string))
        string_permutes: list[str] = [
            "".join(permut) for permut in string_permute_tuple
        ]
        return string_permutes
