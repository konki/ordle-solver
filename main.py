#!/usr/bin/env python3

import sys

from english_words import english_words_set as ews

from solver.ordle_solver import OrdleSolver
from solver.solver_strategy import ContainsStrategy


def main(argv: list[str]):
    try:
        legals = argv[1]
    except IndexError as exc:
        print(f"Exception: {exc}. Too few arguments given.")
        legals = ""

    try:
        illegals = argv[2]
    except IndexError as exc:
        print(f"Exception: {exc}. Too few arguments given.")
        illegals = ""

    solver: OrdleSolver = OrdleSolver(
        solver=ContainsStrategy(legals=legals, illegals=illegals),
        wordlist=list(ews),
    )
    print(solver.find_valids())


if __name__ == "__main__":
    main(sys.argv)
